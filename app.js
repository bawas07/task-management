require('module-alias/register');

// const swaggerUi = require('swagger-ui-express');
const express = require('express');
const cors = require('cors');
const compress = require('compression');
const helmet = require('helmet');
const config = require('config');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const rateLimit = require('express-rate-limit');

const routes = require('./routes/index');

const app = express();

// Set access limit per second
const apiLimiter = rateLimit({
    windowMs: 10 * 60 * 1000,
    max: 100
});

app.use(cookieParser());

// Enable CORS, security, compression, favicon and body parsing
app.enable('trust proxy');
app.use(cors());
app.use(helmet());
app.use(compress());

const green = '\x1b[32m'; 
const noCollor = '\x1b[0m'; 

// generate log when endpoint accessed
app.use(logger(`:date[web] - ${green}info: :method :status :url :response-time ms${noCollor}`));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Host the public folder
app.use('/uploads', apiLimiter, express.static(config.uploads));
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(err, req, res, next) {
    console.log(err);
});

// API Version
app.use('/api/v1', apiLimiter, routes);

module.exports = app;
