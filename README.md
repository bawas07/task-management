# Task Managemenr

This is a Express server for task management 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

you need these software installed on your machine
- Nodejs
- npm
- IDE or editor (Goland or VSCode with GO extention)

### Initial Set-up

- Copy or clone this repository
- run `npm install` to install dependencies needed
- run `node ./bin/www` or `npm start` to run the program locally

### Endpoint Tests

- make sure database are set in file .env
- run `npm test` to run tests

### API Documentation

this project api documentation can be viewed in https://documenter.getpostman.com/view/5262409/SzmZdfzM?version=latest or by importing postman_collection.json file