const path = require('path');

module.exports = {
    host: process.env.HOST || 'localhost',
    port: process.env.PORT || 3000,
    public: 'public',
    uploads: 'public/uploads',
    paginate: {
        default: 10,
        max: 50
    },
    AccessOptions: {
        expiresIn: 365 * 24 * 60 * 60,
        issuer: 'task-management',
        jwtid: 'task.user',
        subject: 'task-access-token',
        algorithm: 'HS256'
    },
    dirPath: {
        log_info_path: path.join(__dirname, '/../logs/infos.log'),
        log_exception_path: path.join(__dirname, '/../logs/exceptions.log'),
        log_default_path: path.join(__dirname, '/../logs/logs.log')
    }
};
  