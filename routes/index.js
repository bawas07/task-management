const express = require('express');
const router = express.Router();
const { helloController, reglogController, taskController } = require('../src/controller');
const {auth} = require('../src/middleware');

/* GET home page. */
router.get('/', helloController.hello);
router.post('/register', reglogController.register);
router.post('/login', reglogController.login);
router.get('/me', auth, reglogController.me);

router.post('/task', auth, taskController.createTask);
router.get('/task', auth, taskController.getTask);

module.exports = router;
