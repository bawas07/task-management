const { response } = require('../helpers');
const { crud, nlp } = require('../service');
const moment = require('moment');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

module.exports = {
    /**
     * Controller when creating task
     */
    createTask: async (req, res) => {

        // Define schema for validation
        const schema = Joi.object({
            str: Joi.array().items(Joi.string()).required(),
        });
        const schemaTask = Joi.object({
            time: Joi.date().required(),
            title:Joi.string().required(),
            location: Joi.string().required()
        });
          
        let schemaTasks = Joi.array().items(schemaTask);

        try {
            await schema.validateAsync(req.body);
            const str = req.body.str;
            const tasks = nlp.getData(str);
            await schemaTasks.validateAsync(tasks);

            const timeOr = [];
            for (let task of tasks) {
                task.userId = req.userData.id;
                timeOr.push({time: task.time});
            }
            const opts = { 
                userId: req.userData.id,
                [Op.or]: timeOr
            };
            const checkDate = await crud.findAllWithOpts('tasks', opts);
            // Check if clashed
            if (checkDate.length > 0) {
                return response({
                    code: 400,
                    message:'potentially clash with these tasks',
                    data: checkDate
                }, null, res);
            }

            const dataTask = await crud.insertBulk('tasks', tasks);
            const result = {
                data: {
                    task: dataTask,
                }, 
            };
            return response(null, result, res);
        } catch (err) {
            return response({data: err.message}, null, res);
        }
    },
    /**
     * Controller when get tasks, place and date can be used as querystring
     */
    getTask: async (req, res) => {
        const schema = Joi.object({
            place: Joi.string(),
            date: Joi.date().format('DD-MM-YYYY')
        });
        try {
            const param = req.query;
            await schema.validateAsync(param);
            const userId = req.userData.id;
            const opts = {
                userId: userId
            };
            if (param.place) {
                opts.location = param.place;
            }
            if (param.date) {
                const startDay = moment(param.date, 'DD-MM-YYYY').startOf('day').utc().format();
                const endDay = moment(param.date, 'DD-MM-YYYY').endOf('day').utc().format();
                opts.time = {
                    [Op.lt]: endDay,
                    [Op.gt]: startDay
                };
            }
            const task = await crud.findAllWithOpts('tasks', opts);
            return response(null, {data:task}, res);
        } catch (err) {
            return response(err, null, res);
        }
    }

};