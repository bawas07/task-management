const { response, jwt } = require('../helpers');
const {crud} = require('../service');
const Joi = require('@hapi/joi').extend(require('@hapi/joi-date'));

module.exports = {
    /**
     * Controller for user's registration
     */
    register: async (req, res) => {
        const schema = Joi.object({
            email: Joi.string().email().required(),
            password: Joi.string().required()
        });
        try {
            await schema.validateAsync(req.body);
            const {email, password} = req.body;
            const user = await crud.create('users', {email, password});
            return response(null, {data: user}, res);
        } catch (err) {
            return response({data: err.message}, null, res);
            
        }
    },
    /**
     * Controller for user's login
     */
    login: async (req, res) => {
        const schema = Joi.object({
            email: Joi.string().email().required(),
            password: Joi.string().required()
        });
        try {
            await schema.validateAsync(req.body);
            const {email, password} = req.body;
            const user = await crud.findOneData('users', {email});
            if (!user) {
                throw new Error('invalid email or password');
            }
            const status = user.validPassword(password);
            if (!status) {
                throw new Error('invalid email or password');
            }
            const payload = JSON.parse(JSON.stringify(user));
            const token = await jwt.createJWT(payload);
            return response(null, {data: token}, res);
        } catch (err) {
            return response({data: err.message}, null, res);
        }
    },
    /**
     * Controller to get user's data
     */
    me: async (req, res) => {
        try {
            const user = await crud.findOneData('users', {id: req.userData.id});
            return response(null, {data: user}, res);
        } catch (err) {
            return response({data: err.message}, null, res);
        }
    }
};