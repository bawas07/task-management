require('module-alias/register');
const response = require('../helpers/response');
const jwtHelpers = require('../helpers/jwt');

const auth = async (req, res, next) => {
    const { authorization } = req.headers;
    if (!authorization) {
        return res
            .status(403)
            .json(response({code: 403, message: 'Authorization header is not present'}));
    }

    try {
        const user = jwtHelpers.verifyJWT(authorization);
        res.local = {};
        req.userData = user;
    } catch (error) {
        return response({code: 401}, null, res);
    }
    next();
};

module.exports = auth;
