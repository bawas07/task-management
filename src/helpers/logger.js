const winston = require('winston');
const winston_daily = require('winston-daily-rotate-file');
const config = require('config');
const loggerPath = config.get('dirPath');

const tsFormat = () => (new Date()).toLocaleTimeString();

// Set log transport
const logOpts = {
    transports: [
        new winston.transports.Console({
            format: winston.format.combine(
                winston.format.prettyPrint(),
                winston.format.timestamp(),
                winston.format.printf(i => `${i.timestamp} | ${i.message}`)
            ),
            level: 'debug',
            handleExceptions: true,
        }),
        new (winston.transports.File)({
            filename: loggerPath.log_exception_path,
            timestamp: tsFormat,
            handleExceptions: true,
            colorize: true,
            level: 'error',
            json: true,
            maxsize: 5242880, // 5MB
            maxFiles: 10
        }),
        new (winston_daily)({
            filename: loggerPath.log_info_path,
            timestamp: tsFormat,
            handleExceptions: false,
            colorize: true,
            json: true,
            maxsize: 5242880, // 5MB
            maxFiles: 100,
            level: 'info',
            datePattern: 'DD-MM-YYYY',
            prepend: true,
            format: winston.format.combine(
                winston.format.prettyPrint(),
                winston.format.splat(),
                winston.format.timestamp(),
                winston.format.printf(i => {
                    return `${i.timestamp} | ${i.message} ${JSON.stringify(i.data)}`;
                }),
            )
        })
    ],
};

const logger = winston.createLogger(logOpts);
module.exports = logger;