const jwt = require('jsonwebtoken');
const config = require('config');

const jwtHelpers = {
    /**
   * Sign new jwt token from passed data.
   * @param {Object} data
   */
    createJWT: async (data) => {
        try {
            const expires = process.env.JWT_EXPIRES || null;
            const access = config.get('AccessOptions');
            if (expires) {
                access.expiresIn = expires;
            }
            if (data.password) {
                delete data.password;
            }
            delete data.createdAt;
            delete data.updatedAt;
            const access_token = await jwt.sign(data, process.env.JWT_SECRET, access);

            const token = {
                token: access_token,
                user: data
            };
            return token;
        } catch (error) {
            throw Error(error.message);
        }
    },

    /**
   * parse JWT with specified options.
   * @param {String} token
   */
    verifyJWT: token => {
        try {
            return jwt.verify(token, process.env.JWT_SECRET, config.get('AccessOptions'));
        } catch (error) {
            if (error.name === 'TokenExpiredError') {
                error.message =
          'Login token expired, please refresh token or do login again.';
            }
            throw Error(error.message);
        }
    },

    /**
   * Parse authorization header
   * @param {String} token
   */
    parseToken: authToken => {
        let token;
        if ((token = authToken.split(' ')[1])) {
            return token;
        }
        throw Error('invalid token');
    },        
};

module.exports = jwtHelpers;
