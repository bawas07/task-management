const message = {
    noData: 'data tidak ditemukan',
    data:  'data ditemukan',
    error: 'terjadi error',
    create: 'data berhasil disimpan',
    update: 'data berhasil dirubah',
    delete: 'data berhasil dihapus',
    validation: 'validasi error',
    process: 'process berhasil'
};

module.exports = message;