const Sugar = require('sugar');
const nlp = require('compromise');
nlp.extend(require('compromise-dates'));
nlp.extend(require('compromise-numbers'));

module.exports = {
    /**
     * Parse sentence and put it into variable
     * @param {string} strArr array of string, where one string is one sentence
     */
    getData: (strArr) => {
        const date = {};
        const resultArr = [];

        for (let str of strArr) {
            const tokens = nlp(str).json();
            let noun = '';
            let verb = '';
            const terms = tokens[0].terms;

            for (let i = 0, iLen = terms.length; i < iLen; i++) {
                // Find the action
                if (terms[i].tags.includes('Verb') && verb == '') {
                    verb = terms[i].text;
                    if (terms[i+1].tags.includes('Noun') || terms[i+1].tags.includes('Adjective') || terms[i+1].tags.includes('Verb')) {
                        verb = verb + ' ' + terms[i+1].text;
                        if (terms[i+1].tags.includes('Gerund') && terms[i+2].tags.includes('Noun')) {
                            verb = verb + ' ' + terms[i+2].text;
                            i++;
                        }
                        i++;
                    }
                    continue;
                }
                // Find the location
                if (terms[i].tags.includes('Noun') && noun == '') {
                    noun = terms[i].text;
                    if (terms[i+1].tags.includes('Noun') || terms[i+1].tags.includes('Adjective')) {
                        noun = noun + ' ' + terms[i+1].text;
                        i++;
                    }
                    continue;
                }

                if (terms[i].tags.includes('Adjective') && noun == '') {
                    noun = terms[i].text;
                    if (terms[i+1].tags.includes('Noun')) {
                        // noun = terms[i+1].text;
                        noun = noun + ' ' + terms[i+1].text;
                        i++;
                    }
                    continue;
                }

                if (terms[i].tags.includes('Preposition') && verb == '') {
                    if (terms[i+1].tags.includes('Noun')) {
                        verb = terms[i+1].text;
                        continue;
                    }
                }
            }
            const result = {
                title: verb,
                location: noun,
            };

            // Find the time and date
            const dateArr = nlp(str).dates().json();
            const dateStr = dateArr[0].text.replace(/[^a-zA-Z0-9 ]/g, '');
            let time = Sugar.Date.create(dateStr);
            if (time == 'Invalid Date') {
                time = Sugar.Date.create(date.full + ' ' + dateStr);
            } else {
                date.month = time.getMonth() + 1; 
                date.date = time.getDate();
                date.year = time.getFullYear();
                date.full = `${date.year}-${date.month}-${date.date}`;
            }
            result.time = time;
            resultArr.push(result);
        }
        return resultArr;
    }
};