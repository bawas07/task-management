'use strict';
const bcrypt = require('bcrypt');

module.exports = {
    up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:s
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
        const seeds = [
            {
                email: 'jest@user.com',
                password: await bcrypt.hash('123123', bcrypt.genSaltSync(8)),
                createdAt: new Date(),
                updatedAt: new Date()
            }
        ];
        return queryInterface.bulkInsert('users', seeds, {});
    },

    down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    }
};
