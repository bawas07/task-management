'use strict';
const moment = require('moment')

module.exports = (sequelize, DataTypes) => {
    const tasks = sequelize.define('tasks', {
        title: DataTypes.STRING,
        location: DataTypes.STRING,
        time: DataTypes.DATE,
        userId: DataTypes.INTEGER
    }, {});
    tasks.associate = function(models) {
        tasks.belongsTo(models.users);
    };

    tasks.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
    
        values.time.toJSON = function(){ return moment(this).format(); }
        return values;
    };
    return tasks;
};