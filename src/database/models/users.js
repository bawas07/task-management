'use strict';
const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
    const users = sequelize.define('users', {
    //     id: {
    //         type: DataTypes.INTEGER,
    //         primaryKey: true
    //     },
        email: DataTypes.STRING,
        password: DataTypes.STRING
    }, {
        timestamps: true,
        hooks: {
            beforeCreate: (user) => {
                const salt = bcrypt.genSaltSync();
                user.password = bcrypt.hashSync(user.password, salt);
            }
        },
        instanceMethods: {
        }
    });
    users.associate = function(models) {
        users.hasMany(models.tasks)
    };

    users.prototype.validPassword = function(password) {
        return bcrypt.compareSync(password, this.password);
    };

    users.prototype.generateHash = function(password) {
        return bcrypt.hash(password, bcrypt.genSaltSync(8));
    };

    users.prototype.toJSON =  function () {
        var values = Object.assign({}, this.get());
      
        delete values.password;
        return values;
    };
      
    return users;
};