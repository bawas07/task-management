const request = require('supertest');
const app = require('../app');
describe('Test Hello', () => {
    it('should return hello world', async () => {
        const res = await request(app)
            .get('/api/v1');
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('status', true);
    });
});