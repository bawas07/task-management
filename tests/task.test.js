const request = require('supertest');
const app = require('../app');

let token;

beforeAll(async (done) => {
    const res = await request(app)
        .post('/api/v1/login')
        .send({
            email: 'jest@user.com',
            password: '123123',
        });
    // .end((err, res) => {
    token = res.body.data.token; // save the token!
    done();
    // });
});

describe('Tasks Endpoints', () => {
    test('It should create new tasks', async () => {
        const res = await request(app)
            .post('/api/v1/task')
            .set('Authorization', `${token}`)
            .send({
                str: ['join wedding party at Raffles this Sunday at 9pm']
            });
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('status', true);
        expect(res.body.data.task[0]).toHaveProperty('id');
    });
    it('should get tasks', async () => {
        const res = await request(app)
            .get('/api/v1/task')
            .query({ place: 'Raffles' })
            .set('Authorization', `${token}`);
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('status', true);
        expect(res.body.data[0]).toHaveProperty('id');
    });
});