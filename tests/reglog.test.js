const request = require('supertest');
const app = require('../app');

describe('Register Login Endpoints', () => {
    it('should create a new user', async () => {
        const res = await request(app)
            .post('/api/v1/register')
            .send({
                email: 'user@user.com',
                password: '123123',
            });
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('status', true);
        expect(res.body.data).toHaveProperty('id');
    });
    it('should get a token', async () => {
        const res = await request(app)
            .post('/api/v1/login')
            .send({
                email: 'user@user.com',
                password: '123123',
            });
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('status', true);
        expect(res.body.data).toHaveProperty('token');
    });
});